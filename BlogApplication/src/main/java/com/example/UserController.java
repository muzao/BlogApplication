package com.example;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import model.UserInfo;
import model.UserInfo.User;

@Controller
@RequestMapping("/contact")
public class UserController {
	private UserInfo userInfo;
	
	
	public UserController() {
		super();
		this.userInfo = new UserInfo();
	}


	@GetMapping("/users")
	@ResponseBody
	public String getUsers() {
		UserInfo userInfo = this.userInfo;
		String content = "";
		ArrayList<UserInfo.User> users = userInfo.getUsers();
		for (User user : users) {
			content += user.toString() + "<br/>";
		}
		return content;
	}
	
	
	@GetMapping("/user/get/{userName}")
	@ResponseBody
	public String getUser(@PathVariable String userName) {
		UserInfo userInfo = this.userInfo;
		ArrayList<UserInfo.User> users = userInfo.getUsers();
		for (User user : users) {
			if (user.getName().equals(userName)) {
				return user.toString();
			}
		}
		return "Not found!";
	}
	
	@GetMapping("/user/create/{userName}/{gender}/{email}/{city}")
	@ResponseBody
	public String createUser(@PathVariable String userName,
							 @PathVariable String gender,
							 @PathVariable String email,
							 @PathVariable String city
							 ) {
		int id = 1;
		UserInfo userInfo = this.userInfo;
		ArrayList<UserInfo.User> users = userInfo.getUsers();
		int usersSize = users.size();
		id += usersSize;
		System.out.println("Before total records:" + String.valueOf(usersSize));
		users.add(new UserInfo.User(String.valueOf(id), userName, gender, email, city));
		usersSize = users.size();
		System.out.println("After total records:" + String.valueOf(usersSize));
		return "total records:" + String.valueOf(usersSize);
	}
	
	
	@GetMapping("/user")
	@ResponseBody
	public String getInfo(@RequestParam("id") String id) {
		UserInfo userInfo = this.userInfo;
		ArrayList<UserInfo.User> users = userInfo.getUsers();
		for (User user : users) {
			if (user.getId().equals(id)) {
				return user.toString();
			}
		}
		return "Not found!";
	}
	
}
