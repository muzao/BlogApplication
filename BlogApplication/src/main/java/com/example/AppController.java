package com.example;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/blogs")
public class AppController {
	@GetMapping("/blogs")
	public String blog() {
		return "blog";
	}
	
	@RequestMapping("/create")
	@ResponseBody
	public String create() {
		return "mapping url is /blogs/create";
	}
}
