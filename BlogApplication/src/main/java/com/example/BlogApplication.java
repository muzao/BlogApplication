package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@SpringBootApplication
@Controller
public class BlogApplication {
	
//	@RequestMapping("/hello")
//	@ResponseBody
//	public String greeting() {
//		return "Hello World!";
//	}
//	
//	@RequestMapping("/")
//	@ResponseBody
//	public String index() {
//		return "<html><head><title>Hello World!</title></head><body><h1>Hello World!</h1><p>This is my first web site</p></body></html>";
//	}
	
	public static void main(String[] args) {
		SpringApplication.run(BlogApplication.class, args);
	}
}
