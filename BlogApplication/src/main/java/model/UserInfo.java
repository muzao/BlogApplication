package model;

import java.util.ArrayList;


import data.UserTable;


public class UserInfo {
	public static class User {
		private String id;
		private String name;
		private String gender;
		private String email;
		private String city;
		
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		@Override
		public String toString() {
			return "User [id=" + id + ", name=" + name + ", gender=" + gender + ", email=" + email + ", city=" + city
					+ "]";
		}

		public User(String id, String name, String gender, String email, String city) {
			super();
			this.id = id;
			this.name = name;
			this.gender = gender;
			this.email = email;
			this.city = city;
		}
		
	}
	
	private ArrayList<User> users;
//    private static class StageSingletonHolder {
//        static UserInfo instance = new UserInfo();
//    }
	
	public UserInfo() {
		super();
		this.users = new ArrayList<User>();
		for (int i = 0; i < UserTable.users.length; i++) {
			String id = UserTable.users[i][0];
			String name = UserTable.users[i][1];
			String gender = UserTable.users[i][2];
			String email = UserTable.users[i][3];
			String city = UserTable.users[i][4];
			this.users.add(new User(id, name, gender, email, city));
		}
	}
//	public static UserInfo getInstance() {
//		return StageSingletonHolder.instance;
//	}
	public ArrayList<User> getUsers() {
		return users;
	}
	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}
//	public static void main(String[] args) {
//		UserInfo u = new UserInfo();
//		ArrayList<User> users = u.getUsers();
//		for (User user : users) {
//			System.out.println(user.toString());
//		}
//		
//	}
}
